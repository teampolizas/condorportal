﻿var expresionesRegulares = {
    numeros : /^[0-9]+$/,
    letras : /^[a-zA-Z]+$/,
    letrasConTilde: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/,
    noEspaciado: /([ ]{2,})|[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/,
    email: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/,
    letrasNumeros: /^[a-zA-Z0-9]+$/,
    decimal: /^\d+(\.\d{0,2})?$/,
};

function validacion_form_espec(elemento, validacion){
    switch (validacion){
        case 'nombresConTildeEspacio':
            let bien = true;
            if( validacion_form(elemento, 6, 50, [expresionesRegulares.letrasConTilde], 'No puede tener puntos, simbolos especiales o espacios imnecesarios') == false ) bien = false;
            //if( validacion_form(elemento, 6, 25, [expresionesRegulares.noEspaciado], 'No debe contener espacios imnecesarios') == false ) bien = false;
            //if( bien == false ) show_msj_error(elemento, 'No puede tener puntos, simbolos especiales o espacios imnecesarios');
            return bien;
            break;
        case 'email':
            return validacion_form(elemento, 6, 50, [expresionesRegulares.email], 'No es correo valido');
            break;
        case 'telefono':
            return validacion_form(elemento, 9, 9, [expresionesRegulares.numeros], 'No es un número de telefono valido');
            break;
        case 'celular':
            return validacion_form(elemento, 10, 10, [expresionesRegulares.numeros], 'No es un número de celular valido');
            break;
        case 'cedula':
            if( validacion_form(elemento, 10, 10, [expresionesRegulares.numeros], 'Solo ingrese números') == false ) return false;
            let cedula = $("#"+elemento).val();
            let digitoVerificador = 0;
            let decenaSuperior = 0;
            let sumaProductos = 0;
            let producto = 0;
            cedula = cedula.split("");

            if (cedula.length < 10){
                show_msj_error(elemento,"Longitud Invalida");
                return false;
            }
                    

            $.each(cedula, function(indice, val) {
                if (indice < 9) {
                    if (indice == 0 || indice % 2 == 0) {
                        console.log(val);
                        producto = parseInt(val) * 2;
                    } else {
                        producto = parseInt(val);
                    }
                    if (producto > 9) producto = producto - 9;

                    sumaProductos += parseInt(producto);
                }
            });

            console.log("Suma: "+sumaProductos);

            if (sumaProductos > 9) {
                let arrayDecenaSuperior = String(
                    sumaProductos
                ).split("");
                decenaSuperior =
                    (parseInt(arrayDecenaSuperior[0]) + 1) * 10;
            } else decenaSuperior = 10;

            digitoVerificador =
                parseInt(decenaSuperior) - parseInt(sumaProductos);
            if (digitoVerificador == 10) digitoVerificador = 0;

            console.log(digitoVerificador + " <<>> " + cedula);

            if (digitoVerificador == cedula[9]) return true;
            show_msj_error(elemento,"Invalido");
            return false;
            break;
        case 'ruc':
            let bienRuc = true;
            let ruc = $("#"+elemento).val();
            if(ruc.length!=13) bienRuc = false;
            /*$("#"+elemento).val(ruc.replace("001",""));
            if( validacion_form_espec(elemento, 'cedula') == false  ) bienRuc = false;
            $("#"+elemento).val(ruc);
            if( !(ruc[10] == "0" && ruc[11] == "0" && ruc[12] == "1") ) bienRuc = false;*/
            if( !moduloRUC(ruc,'juridica') ) bienRuc = false;
            if(bienRuc==false){
                var cedulaEnviar = ruc.slice(0, 10);
                bienRuc = validaCedula(cedulaEnviar);
                if(bienRuc==false){
                    show_msj_error(elemento,'Ruc Inválido');
                }
            }
            return bienRuc;
            break;
    }
    return false;
}

var tiempo_show = 200;
function validacion_form(elemento, min, max, expR, mensj=null){
    //ar = ['hayError'=>false, 'errores' => '' ];
    //var tiempo_show = 200;
    //$.each(arreglo, function(i,item){
    if(min>$("#"+elemento).val().length){
        $("#e_"+elemento).html("Tiene un mínimo de longitud "+min);
        $("#e_"+elemento).show(tiempo_show);
        //return {hayError:true, errores: item[0], mensaje: "Tiene un minimo de longitud " +  item[1] };
        return false;
    }
    if(max<$("#"+elemento).val().length){
        $("#e_"+elemento).html("Tiene un máximo de longitud "+max);
        //return {hayError:true, errores: item[0], mensaje: "Tiene un máximo de longitud " +  item[2] };
        $("#e_"+elemento).show(tiempo_show);
        return false;
    }
    if(expR.length>0){
        var temp_reg = true
        $.each(expR, function(i,item2){
            if( ! $("#"+elemento).val().match(item2) ){
                if(mensj!=null &&  mensj!=null) $("#e_"+elemento).html(mensj);
                else $("#e_"+elemento).html("No es válido");
                $("#e_"+elemento).show(tiempo_show);
                temp_reg = false;
            } //return {hayError:true, errores: item[0], mensaje: "No cumples lo requerido " };
        });
        if(temp_reg==false) return false;
    }
    //});
    return true;
}

    function show_msj_error(elemento, msj){
        $("#e_"+elemento).html(msj);
        $("#e_"+elemento).show(tiempo_show);
    }



    function moduloRUC(arr, tipo) {
        var mod = 11;
        console.log('switch antes');
        switch (tipo) {
            case "natural":
                mod = 10;
                //var digitosIniciales = _.slice(arr, 0, 9);
                var digitosIniciales = arr.slice(0, 9);
                var digitoVerificador = parseInt(arr[9]);
                var arrayCoeficientes = [2, 1, 2, 1, 2, 1, 2, 1, 2];
                break;
            case "juridica":
                //var digitosIniciales = _.slice(arr, 0, 9);
                var digitosIniciales = arr.slice(0, 9);
                var digitoVerificador = parseInt(arr[9]);
                var arrayCoeficientes = [4, 3, 2, 7, 6, 5, 4, 3, 2];
                break;
            case "publico":
                //var digitosIniciales = _.slice(arr, 0, 8);
                var digitosIniciales = arr.slice(0, 8);
                var digitoVerificador = parseInt(arr[8]);
                var arrayCoeficientes = [3, 2, 7, 6, 5, 4, 3, 2];
                break;
        }

        var total = 0;

        console.log('>>'+digitosIniciales+'<<'+digitoVerificador);
        console.log('antes each');

        //var arr2 = digitosIniciales.split();

        //$.each(digitosIniciales, function(key, value) {
        for(var key = 0; key<digitosIniciales.length;key++){
            var value = digitosIniciales[key];
            console.log('each');
            var valorPosicion =
                            parseInt(value) * arrayCoeficientes[key];
            /*if (tipo == "natural" && valorPosicion >= 10) {
                valorPosicion = valorPosicion.split("").reduce(
                    function (sum, n) {
                        return sum + parseInt(n);
                    },0
                );
            }*/
            total += valorPosicion;
            console.log('fin each');
            //});
        }

        var residuo = total % mod;
        var resultado = 0;

        if (residuo == 0) resultado = 0;
        else resultado = mod - residuo;

        console.log('hola');

        if (resultado != digitoVerificador) return false;
        return true;
    }

    function validaCedula(cedula){
        let digitoVerificador = 0;
        let decenaSuperior = 0;
        let sumaProductos = 0;
        let producto = 0;
        cedula = cedula.split("");

        if (cedula.length < 10){
            //show_msj_error(elemento,"Longitud Invalida");
            return false;
        }
                    

        $.each(cedula, function(indice, val) {
            if (indice < 9) {
                if (indice == 0 || indice % 2 == 0) {
                    console.log(val);
                    producto = parseInt(val) * 2;
                } else {
                    producto = parseInt(val);
                }
                if (producto > 9) producto = producto - 9;

                sumaProductos += parseInt(producto);
            }
        });

        console.log("Suma: "+sumaProductos);

        if (sumaProductos > 9) {
            let arrayDecenaSuperior = String(
                sumaProductos
            ).split("");
            decenaSuperior =
                (parseInt(arrayDecenaSuperior[0]) + 1) * 10;
        } else decenaSuperior = 10;

        digitoVerificador =
            parseInt(decenaSuperior) - parseInt(sumaProductos);
        if (digitoVerificador == 10) digitoVerificador = 0;

        console.log(digitoVerificador + " <<>> " + cedula);

        if (digitoVerificador == cedula[9]) return true;
        //show_msj_error(elemento,"Invalido");
        return false;
    }