﻿//Para Llenar JQGrid
function ConsultaMensajeError(numPoliza, tipoPoliza, IdAseguradora, Identificacion) {
    $.ajax({
        url: '/Usuario/ConsultaErrorPoliza',
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: {
            'numPoliza': numPoliza,
            'tipoPoliza': tipoPoliza,
            'IdAseguradora': IdAseguradora,
            'identificacion': Identificacion
        },
        success: function (mensaje) {
            mensaje = "<p align=justify>" + mensaje + "</p>";
            $('#ErrorDetalle').html(mensaje);
        },
        error: function () {
            //alert('Error En La Comunicación');
            //mostrarMensaje('Error En La Comunicación');
            alertaFlotante("error", "Error En La Comunicación");
        }
    });
}

function LlenaPopup(Parametros) {
    var ListaParametro = new Array();
    ListaParametro = Parametros.split("-");
    $("#txtAsegurado").val(ListaParametro[0]);
    $("#txtDireccion").val(ListaParametro[1]);
    $("#txtTelefono").val(ListaParametro[2]);
    $("#txtRamo").val(ListaParametro[3]);
    $("#txtValor").val(ListaParametro[4]);
    $("#txtFechaInicio").val(ListaParametro[5]);
    $("#txtFechaFin").val(ListaParametro[6]);
    $("#txtIdPoliza").val(ListaParametro[7]);

    $('#popupDescripcion').modal('toggle')
}

function popup(PopupNombre, idPoliza, nombreDocumento) {
    $("#txtIdPoliza").val(idPoliza);
    $("#txtNombrePoliza").val(nombreDocumento);
    $(PopupNombre).modal('toggle');
    $("#body_clientes_principal").css("padding-right", "0");
}

function popupOtrosDocumentos(PopupNombre, idPoliza, nombreDocumento) {
    $("#txtIdPoliza").val(idPoliza);
    $("#txtNombreDoc").val(nombreDocumento);
    $(PopupNombre).modal('toggle');
}

function popupError(PopupNombre, parametros) {
    var datos = parametros.split('-');
    ConsultaMensajeError(datos[0], datos[1], datos[2], datos[3]);

    $(PopupNombre).modal('toggle');
}

function popupErrorFirma(PopupNombre)
{
    $(PopupNombre).modal('toggle');
}

function validaNumero(evento) {//permite digitar solo numeros
    //---------------------------------------------------------------------------------------//  
    var key = validaEvento(evento);
    if (key == 8) { return true; }

    if (((key > 0 && key < 48) || (key > 57 && key < 255)) && (key != 46)) {
        if (window.event) {
            evento.keyCode = 0;
            return false;

        }
        else if (evento.which) {
            return false;
        }
    }
}

function validaEvento(e) {
    var keynum;
    if (window.event)
    { keynum = e.keyCode; }
    else if (e.which)
    { keynum = e.which; }
    return keynum;
}

function comprobarFecha() {
    var retorno = true;
    var fecha = $('#txtFechaInicio').val();
    var fecha2 = $('#txtFechaFin').val();
    //las fechas que recibo la descompongo en un array 
    var array_fecha = fecha.split("/");
    var array_fecha2 = fecha2.split("/");
    //si el array no tiene tres partes, la fecha es incorrecta 
    if (array_fecha.length != 1 || array_fecha2.length != 1) {
        if (array_fecha.length != 3) {
            //alert('Ingresar Fecha Inicial');
            //mostrarMensaje('Ingresar Fecha Inicial');
            alertaFlotante("warning", 'Ingresar Fecha Inicial');
            return false;
        }
        if (array_fecha2.length != 3) {
            //alert('Ingresar Fecha Final');
            //mostrarMensaje('Ingresar Fecha Final');
            alertaFlotante("warning", 'Ingresar Fecha Final');
            return false;
        }
    }
    else {
        return true;
    }

    //compruebo que los anios, mes, dia son correctos
    var dia;
    dia = parseInt(array_fecha[0], 10);
    if (isNaN(dia)) {
        return false;
    }
    var dia2;
    dia2 = parseInt(array_fecha2[0], 10);
    if (isNaN(dia2)) {
        return false;
    }
    var mes;
    mes = parseInt(array_fecha[1], 10);
    if (isNaN(mes)) {
        return false
    }
    var mes2;
    mes2 = parseInt(array_fecha2[1], 10);
    if (isNaN(mes2)) {
        return false
    }
    var anio;
    anio = parseInt(array_fecha[2], 10);
    if (isNaN(anio)) {
        return false;
    }
    var anio2;
    anio2 = parseInt(array_fecha2[2], 10);
    if (isNaN(anio2)) {
        return false;
    }

    if (anio > anio2) {
        retorno = -1;
    }
    if (anio2 > anio) {
        retorno = 1;
    }

    if (anio == anio2) {
        retorno = 0;
        if (mes > mes2) {
            retorno = -1;
        }
        if (mes2 > mes) {
            retorno = 1;
        }
        if (mes == mes2) {
            retorno = 0;
            if (dia > dia2) {
                retorno = -1;
            }
            if (dia2 > dia) {
                retorno = 1;
            }
            if (dia == dia2) {
                retorno = 0;
            }
        }//end if mes == mes2		
    }//end if anio == anio2
    if (retorno == 1 || retorno == 0) {
        return true;
    }
    else {
        //alert("'Fecha Inicio' Debe Ser Mayor A 'Fecha Fin'");
        //mostrarMensaje("Fecha Inicio' Debe Ser Mayor A 'Fecha Fin");
        alertaFlotante("warning", "Fecha Inicio' Debe Ser Mayor A 'Fecha Fin");
        return;
    }
}

function CadenaCheckSeleccionados(formulario) {
    var cadena = '';
    $("input[type=checkbox]:checked").each(function () {
        //cada elemento seleccionado
        /*if (cadena == "") {
            cadena = cadena + $(this).val();
        }
        else {
            cadena = cadena + "|" + $(this).val();
        }*/
        if ($(this).val() != "" && $(this).val().length>0) {
            if (cadena == "") {
                cadena = cadena + $(this).val();
            }
            else {
                cadena = cadena + "|" + $(this).val();
            }
        }
    });
    return cadena;
}

function EnviaCorreo() {
    var parametros = CadenaCheckSeleccionados('frmGrid');
    if (parametros == "") {
        //alert('Debe Marcar al Menos 1 Documento Para Enviar el Mail');
        //mostrarMensaje('Debe Marcar al Menos 1 Documento Para Enviar el Mail');
        alertaFlotante("error", 'Debe Marcar al Menos 1 Documento Para Enviar el Mail');
        return;
    }
    $(".loader").css("display", "block");
    $.ajax({
        url: '/Usuario/EnviaMail',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {
            'parametros': parametros
        },
        success: function (mensaje) {
            //alert(mensaje);
            //mostrarMensaje(mensaje);
            if(!(mensaje.indexOf("error")>-1)) alertaFlotante("success", mensaje);
            else alertaFlotante("error", mensaje);
            //console.log(mensaje);
        },
        error: function () {
            //alert('Error En La Comunicación');
            //mostrarMensaje('Error En La Comunicación');
            alertaFlotante("error", 'Error En La Comunicación');
        },
        complete: function () {
            $(".loader").css("display", "none");
        }
    });
}

function validaLetras(evento) {//Permite validar solo letras
    var key = validaEvento(evento);
    if (key == 32) {
        if (window.event) {
            evento.keyCode = 0;
            respuesta = false;
            return respuesta;
        }
        else if (evento.which) {
            return false;
        }//end if window.event
    }

    if ((key < 48 || key > 57)) {
        //if ((key < 65 || key > 90) && (key < 97 || key > 122) && (key != 32) && (key != 8)) {
        if ((key < 65 && key > 90) || (key < 97 && key > 122) || (key == 32) || (key == 8) || (key == 45)) {

            if (window.event) {
                evento.keyCode = 0;
                respuesta = false;
                return true;
            }
            else if (evento.which) {
                return false;
            }//end if window.event
        }//end if key < 65
    }//end if key < 48
}//end function validaNumero

function LevantaPopupGeneral(nombrePopup) {
    $(nombrePopup).modal('toggle');
}

function FirmaAseguradora(idPoliza) {
    $.ajax({
        url: '/FirmaElectronica/FirmaElectronicaAseguradora',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {
            'idPoliza': idPoliza
        },
        success: function (mensaje) {
            //alert(mensaje);
            //mostrarMensaje(mensaje);
            if(!(mensaje.indexOf("error")>-1)) alertaFlotante("success", mensaje);
            else alertaFlotante("error", mensaje);
        },
        error: function () {
            //alert('Error En La Comunicación');
            //mostrarMensaje('Error En La Comunicación');
            alertaFlotante("success", 'Error En La Comunicación');
        }
    });
}

function FirmaCliente(opcion) {
    $(".loader").css("display", "block");
    var dataNombre = "";
    if (opcion == 'P')
        dataNombre = $("#txtNombrePoliza").val();
    else {
        dataNombre = $("#txtNombreDoc").val();
    }

    if ($("#txtCodigoVerificacion").val() != "") {
        try{
            $.ajax({
                url: '/FirmaElectronica/FirmaElectronica/',
                type: 'POST',
                dataType: 'json',
                data: {
                    'idPoliza': $("#txtIdPoliza").val(),
                    'txtCodigoVerificacion': $("#txtCodigoVerificacion").val(),
                    'txNombreDoc': dataNombre
                },
                success: function (mensaje) {
                    //alert(mensaje);
                    //mostrarMensaje(mensaje);
                    if(!(mensaje.indexOf("error")>-1) && mensaje.length>0) alertaFlotante("success", mensaje);
                    else alertaFlotante("error", mensaje);
                    $("#txtCodigoVerificacion").val("");
                    $('#popupFirma').modal('hide');
                    if (opcion == "O") {
                        CargaDatosOtrosDocumentosClientes();
                    } else {
                        CargaDatosClientes();
                    }

                    reloadFiltros();
                },
                error: function () {
                    //alert('Error En La Comunicación');
                    //mostrarMensaje('Error En La Comunicación');
                    alertaFlotante("error", 'Error En La Comunicación');
                },
                complete: function () {
                    if (menuSeleccionado != null && menuSeleccionado != "") {
                        document.querySelector('a[data-page="' + menuSeleccionado + '"]').click();
                    }
                    $(".loader").css("display", "none");
                }
            });
        }catch(ex){
            $(".loader").css("display", "none");
            alertaFlotante("error", "Ocurrio un error");
        }
    } else {
        //mostrarMensaje("Por favor ingrese el token de firma.");
        alertaFlotante("error", "Por favor ingrese el token de firma.");
        $(".loader").css("display", "none");
        //alert("Por favor ingrese el token de firma");
    }
    //$(".loader").css("display", "none");
}

var segun = 0;
var minutos = 0;
function generarTokenWeb() {
        
    $("#btn_generar_token_web").attr('disabled', 'disabled');
    minutos = minutosEsperaToken;
    segun = segundosEsperaToken;
    //$("#btn_generar_token_web").val(minutosEsperaToken+":"segun);
    $(".loader").css("display", "block");
        $.ajax({
            url: '/FirmaElectronica/generaTokenWeb/',
            type: 'POST',
            dataType: 'json',
            data: {
            },
            success: function (mensaje) {
                //alert(mensaje);
                if (mensaje != "" && mensaje.length == 9) {
                    $("#txtCodigoVerificacion").val(mensaje);
                } else {
                    //alert("Error al obtener Token");
                    //mostrarMensaje("Error al obtener Token");
                    alertaFlotante("error", "Error al obtener Token");
                }
            },
            error: function () {
                //alert('Error En La Comunicación');
                //mostrarMensaje("Error En La Comunicación");
                alertaFlotante("error", "Error En La Comunicación");
            },
            complete: function () {
                $(".loader").css("display", "none");
            }
            
        });
}

$(document).ready(function () {
    setInterval(bloquear_firmador_web, 1000);
});

function bloquear_firmador_web() {
    if ( minutos > 0 || segun > 0) {
        if (segun > 0) segun = segun - 1;
        else {
            minutos = minutos - 1;
            segun = 59;
        }

        $("#btn_generar_token_web").val(minutos + ":" + segun);
        if (segun < 10) $("#btn_generar_token_web").val(minutos + ":0" + segun);

    } else {
        $("#btn_generar_token_web").removeAttr("disabled");
        $("#btn_generar_token_web").val("Generar Código");
    }
}


function CargaDatosClientes() {
    $(".loader").css("display", "block");
    $.ajax({
        url: '/Clientes/CargarGrid/',
        type: 'POST',
        dataType: "html",
        async: false,
        success: function (data) {
            $('#divGrigCliente').html(data);
            $('body').css("padding-right", "0" );
            $(".loader").css("display", "none");
            reloadFiltros();
        },
        error: function () {
            //alert('Error En La Comunicación');
            //mostrarMensaje("Error en la Comunicación");
            alertaFlotante("error", "Error En La Comunicación");
        }
    });
}

function CargaDatosOtrosDocumentosClientes() {
    $(".loader").css("display", "block");
    $.ajax({
        url: '/Clientes/CargarGridOtrosDocumentos/',
        type: 'POST',
        dataType: "html",
        async: false,
        success: function (data) {
            
            $('#divGrigClienteOtrosDoc').html(data);
            $(".loader").css("display", "none");
            reloadFiltros();
        },
        error: function () {
            //alert('Error En La Comunicación');
            //mostrarMensaje("Error en la Comunicación");
            alertaFlotante("error", "Error En La Comunicación");
        }
    });
}

function reloadFiltros() {
    /*$('table').footable().bind('footable_filtering', function (e) {
        var selected = $('.filter-documents').find(':selected').text();
        if (selected && selected.length > 0 && selected != "Pólizas Electronicas" && selected != "Seleccione tipo estado póliza") {
            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
            e.clear = !e.filter;
        }
    });
    $('.clear-filter').click(function (e) {
        e.preventDefault();
        $('.filter-documents').val('');
        $('table.demo').trigger('footable_clear_filter');
    });

    $('.filter-documents').change(function (e) {
        e.preventDefault();
        $('table.demo').trigger('footable_filter', { filter: $('#filter').val() });
    });*/

    $('#change-page-size').change(function (e) {
        e.preventDefault();
        var pageSize = $(this).val();
        $('.footable').data('page-size', pageSize);
        $('.footable').trigger('footable_initialized');
    });

    $('#change-nav-size').change(function (e) {
        e.preventDefault();
        var navSize = $(this).val();
        $('.footable').data('limit-navigation', navSize);
        $('.footable').trigger('footable_initialized');
    });
    $('#body_clientes_principal').css("padding-right", "0");
}

function consultaSmtp() {
    $.ajax({
        url: '/Admin/ConsultaSMTP',
        type: 'POST',
        dataType: 'json',
        cache: false,
        async: false,
        data: { 'idAseguradora': $("#cmbCompania").val() },
        success: function (data) {
            data = data.split('|');
            if (data[0] == "OK") {
                $("#txtHost").val(data[1]);
                $("#txtPuerto").val(data[2]);
                $("#cmbSSL").val(data[3]);
                $("#txtEmailCredencial").val(data[4]);
                $("#txtPassword").val(data[5]);
                $("#txtEmailAdressFrom").val(data[6]);
                $("#txtTo").val(data[7]);
                $("#txtCC").val(data[8]);
                $("#txtAsunto").val(data[9]);
            }
            else
                //mostrarMensaje(data[0]);
                alertaFlotante("success", data[0]);
                //alert(data[0]);
        }
    });
}













/////////////////////// Imagen Menu
var opcion_menu = "grande";
var tam_anterior = 0;
var tiempo_rapidez_menu_img_intervalo = 50;
function imagen_menu(time, accion) {
    //if ($("body").attr("class") == "sidebar-mini layout-fixed sidebar-open") {
    /*if ($("aside").width() >= "175") {
        $("#img_menu_normal").show(time);
        $("#img_menu_icono").hide(time);
    } else {
        $("#img_menu_normal").hide(time);
        $("#img_menu_icono").show(time);
    }*/
    if (accion == 1) {
        tam_anterior = $("aside").width();
    }
    else {
        //console.log($("aside").width() + " >"+ tam_anterior);
        if (parseInt($("aside").width()) > parseInt(tam_anterior)) {
            ocultar_por_tam(time, "grande");
        }
        else {
            if (parseInt($("aside").width()) < parseInt(tam_anterior)) {
                ocultar_por_tam(time, "chico");
            }
        }
        tam_anterior = $("aside").width();
    }
    //console.log("accion: "+accion);
    //console.log($("aside").width());
}
function ocultar_por_tam(time,accion) {
    //if ($("body").attr("class") == "sidebar-mini layout-fixed sidebar-open") {
    if (accion == "grande") {
        $(".img_men").css("max-height", "none");
        $("#img_menu_normal").show(time);
        $("#img_menu_icono").hide(time);
    } else {
        $(".img_men").css("max-height", $(".img_men").height());
        $("#img_menu_icono").show(time);
        $("#img_menu_normal").hide(time);
    }
    //console.log(accion);
}

function accion_aside() {
    imagen_menu(950, 1);
    //setTimeout('imagen_menu(950,2)', 30);
    setTimeout('imagen_menu(1,2)', tiempo_rapidez_menu_img_intervalo);
}


$(document).ready(function () {
    imagen_menu(0, 1);
    //$(".img_men").css("max-height", $("#contenedor_img_menu").height());
    $(".nav-item .nav-link").on("click", function () {
        //$("#contenedor_img_menu a").css("max-height", $("#contenedor_img_menu a").height());
        //$("#contenedor_img_menu").css("max-height", $("#contenedor_img_menu").height());
        //$(".img_men").css("max-height", $("#contenedor_img_menu").height());
        accion_aside();
    });

    //$("aside").on("change", function () {
    $("aside").on("mouseenter", function () {
        accion_aside();
    });
    $("aside").on("mouseleave", function () {
        accion_aside();
    });

});


///////////////////////////////////////////////////////////////////////////////////
////////////////////////////Alertas flotantes
function alertaFlotante(tipoAlerta, mensaje, titulo=null){
    var opcionesT = {
        "closeButton": true,
        "debug": true,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    var opcionesTfull = {
        "closeButton": true,
        "debug": true,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    switch(tipoAlerta){
        case "success":
            if(titulo==null) titulo = "Correcto";
            break;
        case "warning":
            if(titulo==null) titulo = "Advertencia";
            break;
        case "error":
            if(titulo==null) titulo = "Error";
            break;
        case "success_full":
            if(titulo==null) titulo = "Correcto";
            tipoAlerta = "success";
            opcionesT = opcionesTfull;
            break;
        case "error_full":
            if(titulo==null) titulo = "Error";
            tipoAlerta = "error";
            opcionesT = opcionesTfull;
            break;
    }
    toastr[tipoAlerta](mensaje, titulo, opcionesT);
}
